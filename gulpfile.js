
var gulp = require('gulp');

// plugins
var connect = require('gulp-connect');


gulp.task('connect', function () {
  connect.server({
    root: 'public/',
    port: 8888,
    livereload: true
  });
});

gulp.task('watch', function () {
  gulp.watch(['./public/*.html'], ['html']);
});

gulp.task('default', ['connect', 'watch']);
