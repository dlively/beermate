var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var BreweryDb = require('brewerydb-node');

//change me
var KEY = '5f275fd14545c371d0cb26972d24faf6';//dont ever do this!!!!!!!!
var brewService = new BreweryDb(KEY);

//setup middleware
app.use(bodyParser.json());
app.use(express.static( path.join(__dirname, 'public')));

/**
 * Setup routes
 */

app.get('/', function(req, res) {
    res.sendFile('index.html');
});

app.get('/api/search/:beer', function (req, res) {

    console.log(req.params.beer);

    brewService.search.beers({q: req.params.beer}, function(err, results) {

        if(err) {

          console.error(err);
          res.status(500).json({
            errorMsg: err.message
          });

        }

        res.json(results);

  });


});

app.get('/api/beer/:id', function(req, res) {

    brewService.beer.getById(req.params.id, {}, function(err, result) {

        if(err) {

          console.error(err);
          res.status(500).json({
            errorMsg: err.message
          });

        }

        res.json(result);

    });

});


// START THE SERVER
app.listen(8080);
console.log('server running : http://localhost:8080');
