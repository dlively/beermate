(function () {

'use strict';


  angular.module('beerMateApp', ['ngRoute', 'ngAnimate'])

  .config([
    '$locationProvider',
    '$routeProvider',
    function($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');
      // routes
      $routeProvider
        .when("/", {
          templateUrl: "./templates/main.html",
          controller: 'BeerListCtrl',
          reloadOnSearch: false
        })
        .when("/beer/:id", {
            templateUrl: "./templates/detail.html",
            controller: 'BeerDetailCtrl'
        })
        .otherwise({
           redirectTo: '/'
        });
    }
  ])

  .controller('BeerListCtrl', function($scope, $route, $routeParams, $location, beerService) {

      $scope.beers = [];

      function getBeerList(beerName) {
          console.log('searching for :' + beerName);

          beerService.search(beerName)
            .success(function(beers) {
                console.log(beers);

                $scope.beers = beers;
            })
            .error(function(err) {
                console.error(err);
            });
      }

      if($routeParams.q) {
          getBeerList($routeParams.q);
      }

      $scope.find = function() {
          $location.search('q', $scope.search);
          getBeerList($scope.search);
      }

  })

  .controller('BeerDetailCtrl', function($scope, $route, $routeParams, beerService) {

      console.log($routeParams);

      $scope.beer = {};
     //call beer server to retrieve the beer details

  })

  .service('beerService', function($http) {

      var baseUrl = '/api/';
      var beerService = {};

      this.search = function(beerName) {
          return $http.get(baseUrl + 'search/' + beerName, {cache: true});
      }

      this.getBeer = function(id) {
          //implement retrieval of single beer based on its id
      }

  })

}());
